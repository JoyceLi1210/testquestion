
function BtClick() {
    $("#slider_width").val(400);
    $("#slider_height").val(150);

    $("#slider_width").css ({
        "background": 'linear-gradient(to right, #FF7300 0%, #FF7300 70%, #fff 70%, #fff 100%)'
    })
    $("#slider_height").css ({
        "background": 'linear-gradient(to right, #FF7300 0%, #FF7300 40%, #fff 40%, #fff 100%)'
    })
}  

function adjustButton() {
    const slider_width = $("#slider_width").val();
    const slider_height = $("#slider_height").val();
    const output_text = slider_width +" X " +slider_height;

    $("#text").text(output_text);
    $("#button").css("width", slider_width);
    $("#button").css("height", slider_height);
}

function sliderWidth(e) {
    const value = (e.value-e.min)/(e.max-e.min)*100;
    
    $(e).css ({
        "background": 'linear-gradient(to right, #FF7300 0%, #FF7300 ' + value + '%, #fff ' + value + '%, white 100%)'
    })

    adjustButton();
}

function sliderHeight(e) {
    const value = (e.value-e.min)/(e.max-e.min)*100;
    
    $(e).css ({
        "background": 'linear-gradient(to right, #FF7300 0%, #FF7300 ' + value + '%, #fff ' + value + '%, white 100%)'
    })

    adjustButton();
}

    