
function columnWidth(e) {
    const colWidth = $(e).val();
    
    $(".box_container > div").css({
        "flex" : "0 0 " + colWidth + "px"
    })  
    $(".icon_container > div").css({
        "flex" : "0 0 " + colWidth + "px"
    })
} 

function horizontalGap(e) {
    const HGap = $(e).val();
    
    $(".box_container > div").css({
        "padding-right" : HGap + "px"
    })
    $(".icon_container > div").css({
        "padding-right" : HGap + "px"
    })
} 

function ColumnCount(e) {
    const colCount = $(e).val();
    
    $(".box_container > div").css({
        "flex" : "0 0 " + (100 / colCount) + "%"
    })
    $(".icon_container > div").css({
        "flex" : "0 0 " + (100 / colCount) + "%"
    })
}

function rowHeight(e) {
    const height = $(e).val();
    
    $(".box").css({
        "height" : height
    })
    $(".icon_box").css({
        "height" : height
    })
}
    
function verticalGap(e) {
    const vGap = $(e).val();
    
    $(".box").css({
        "margin-bottom" : vGap + "px"
    })
    $(".icon_box").css({
        "margin-bottom" : vGap + "px"
    })
}   
    
function click_left() { 
    $(".title_container_left").css({
        "background-color" : "#C7DEF5", 
        "color" : "#1259A1"
    })
    $(".title_container_right").css({
        "background-color" : "white", 
        "color" : "black"
    })
    
    $(".box_container").css({
        "display" : "flex"
    })
    $(".scroll_control").css({
        "display" : "none"
    })

    $(".left_title_select").css({
        "border-bottom" : "3px solid #CFCFCF"
    })
    $(".right_title_select").css({
        "border-bottom" : "none"
    })
}

function click_right() {
    $(".title_container_left").css({
        "background-color" : "white", 
        "color" : "black"
    })
    $(".title_container_right").css({
        "background-color" : "#C7DEF5",
        "color" : "#1259A1"
    })
       
    $(".box_container").css({
        "display" : "none"
    })
    $(".scroll_control").css({
        "display" : "flex"
    })
    
    $(".right_title_select").css({
        "border-bottom" : "3px solid #CFCFCF"
    })
    $(".left_title_select").css({
        "border-bottom" : "none"
    })
}